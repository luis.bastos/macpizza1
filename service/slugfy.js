function slugify(string) {
    const a = ' àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
    const b = '-aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
    const p = new RegExp(a.split('').join('|'), 'g')

    string = string.toString().toLowerCase()
        .replace(/\s+/g, '-') 
        .replace(p, c => b.charAt(a.indexOf(c))) 
        .replace(/&/g, '-and-') 
        .replace(/\-\-+/g, '-') 
        .replace(/^-+/, '') 
        .replace(/-+$/, '') 
    return `${string}`
}


module.exports = slugify
